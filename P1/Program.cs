﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Основные типы:
            //int i целое число
            //float f число с дробным остатком
            //string str строка (текст)
            //bool bl логический оператор (формула)
            //Целочисленные типы
            //Операторы: + - * / %
            //Логические операторы == != > >= <= <
            //Интерполяция переменной типа string с помощью $ и {}
            //Логическое И &&
            //Логическое ИЛИ ||
            //Логические if else (для небольшого числа условий), switch, case, break, default (для большого числа условий)
            //Циклы while (для неопределенных) for (для определенных)
            //Ctrl+K+D выравнивание отступов

            Random rand = new Random();

            int[] sectors = { 6, 28, 15, 15, 17 };
            bool isOpen = true;

            while (isOpen)
            {
                Console.SetCursorPosition(0, 18);
                for (int i = 0; i < sectors.Length; i++)
                {
                    Console.WriteLine($"In sector {i + 1} {sectors[i]} seats is allowed");
                }

                Console.SetCursorPosition(0, 0);
                Console.WriteLine("Race registraition");
                Console.WriteLine("1 - book the seat" +
                    " 2 - close the program");
                Console.WriteLine("Enter the command");

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 1:
                        int userSector, userPlaceAmount;
                        Console.WriteLine("In which sector do you want to fly?");
                        userSector = Convert.ToInt32(Console.ReadLine()) - 1;
                        if (sectors.Length <= userSector || userSector < 0)
                        {
                            Console.WriteLine("Where is no sector with this number");
                            break;
                        }
                        Console.WriteLine("How much seats do you want to book?");
                        userPlaceAmount = Convert.ToInt32(Console.ReadLine());
                        if (sectors[userSector] < userPlaceAmount || userPlaceAmount < 0)
                        {
                            Console.WriteLine($"Not enought seats in {userSector} sector." +
                                $"In {userSector} sector allowed only {sectors[userSector]} seats");
                            break;
                        }
                        sectors[userSector] -= userPlaceAmount;
                        break;
                    case 2:
                        isOpen = false;
                        break;
                }


                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
